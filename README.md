# Checkout coding exercise
This is a Gelato coding exercise done by Anton Sidorov  

By default data contains three products
- A with ptice 10
- B with ptice 100 
- C with ptice 25

and 2 companies
- Company A (A x 3 items for 25)
- Company B (B x 2 items for 150)


### Installing

Run docker and init project once: 
```
 make init
```

make sure the project is up and healthy, run tests:
```
 make tests
```

for start and stop the project use:
```
 make up
 make down
```

###Usage
Enter the running app image
```
 make sh
```
run the checkout command
```
 php bin/console user:checkout
```
enter the items for checkout in the format A,B,C etc.
Press enter to see the result

