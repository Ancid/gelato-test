init:
	@docker-compose up --build -d
	@sleep 10
	@docker-compose exec php composer install
	@docker-compose exec php cp .env.dist .env
	@docker-compose exec php php bin/console doctrine:database:create --if-not-exists
	@docker-compose exec php php bin/console doctrine:schema:create -n
	@docker-compose exec php php bin/console doctrine:fixtures:load -n

up:
	@docker-compose up -d

down:
	@docker-compose down

remove:
	@docker-compose down -v

tests:
	@docker-compose exec php php bin/phpunit

sh:
	@docker-compose exec php sh
