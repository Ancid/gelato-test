<?php declare(strict_types=1);

namespace App\Tests;

use App\Domain\CheckoutService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CheckoutTest extends KernelTestCase
{
    /**
     * @return CheckoutService
     */
    private function bootCheckoutService(): CheckoutService
    {
        self::bootKernel();
        $container = self::$container;
        /** @var CheckoutService $checkoutService */
        $checkoutService = $container->get(CheckoutService::class);

        return $checkoutService;
    }

    public function testScenarioUserEntersZeroItem(): void
    {
        $checkoutService = $this->bootCheckoutService();
        $chekoutIitems = '';
        $checkout = $checkoutService->processCheckout($chekoutIitems);
        $resultItems = $checkout->getItems();

        $this->assertIsArray($resultItems);
        $this->assertCount(0, $resultItems);
        $this->assertEquals('0', $checkout->getTotal());
    }

    public function testScenarioUserEnters1Item(): void
    {
        $checkoutService = $this->bootCheckoutService();

        $chekoutIitems = 'A';
        $checkout = $checkoutService->processCheckout($chekoutIitems);
        $resultItems = $checkout->getItems();

        $this->assertIsArray($resultItems);
        $this->assertArrayHasKey('A', $resultItems);
        $this->assertEquals(1, $resultItems['A']);
        $this->assertEquals('10', $checkout->getTotal());
    }

    public function testScenarioUserEntersSeveralItems(): void
    {
        $checkoutService = $this->bootCheckoutService();

        $chekoutIitems = 'A,B,C,B';
        $checkout = $checkoutService->processCheckout($chekoutIitems);
        $resultItems = $checkout->getItems();

        $this->assertIsArray($resultItems);
        $this->assertArrayHasKey('A', $resultItems);
        $this->assertEquals(1, $resultItems['A']);
        $this->assertArrayHasKey('B', $resultItems);
        $this->assertEquals(2, $resultItems['B']);
        $this->assertArrayHasKey('C', $resultItems);
        $this->assertEquals(1, $resultItems['C']);
        $this->assertEquals('235', $checkout->getTotal());
    }

    public function testScenarioUserUsesCompany()
    {
        $checkoutService = $this->bootCheckoutService();

        $chekoutIitems = 'A,B,C,B';
        $companies = ['Company B'];
        $checkout = $checkoutService->processCheckout($chekoutIitems, $companies);
        $resultItems = $checkout->getItems();

        $this->assertIsArray($resultItems);
        $this->assertArrayHasKey('A', $resultItems);
        $this->assertEquals(1, $resultItems['A']);
        $this->assertArrayHasKey('B', $resultItems);
        $this->assertEquals(2, $resultItems['B']);
        $this->assertArrayHasKey('C', $resultItems);
        $this->assertEquals(1, $resultItems['C']);
        $this->assertEquals('185', $checkout->getTotal());
    }

    public function testScenarioUserUsesMultipleCompanies()
    {
        $checkoutService = $this->bootCheckoutService();

        $chekoutIitems = 'A,B,B,A,A,C,B,B';
        $companies = ['Company B', 'Company A'];
        $checkout = $checkoutService->processCheckout($chekoutIitems, $companies);
        $resultItems = $checkout->getItems();

        $this->assertIsArray($resultItems);
        $this->assertArrayHasKey('A', $resultItems);
        $this->assertEquals(3, $resultItems['A']);
        $this->assertArrayHasKey('B', $resultItems);
        $this->assertEquals(4, $resultItems['B']);
        $this->assertArrayHasKey('C', $resultItems);
        $this->assertEquals(1, $resultItems['C']);
        $this->assertEquals('350', $checkout->getTotal());
    }
}
