<?php declare(strict_types=1);

namespace App\Domain;

use App\Entity\Item;
use Doctrine\ORM\EntityManagerInterface;

class CheckoutService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $items
     * @param string[]|null $companies
     * @return Checkout
     */
    public function processCheckout(string $items, array $companies = []): Checkout
    {
        $checkout = new Checkout($this->em, $companies);
        $itemsArray = explode(',', $items);
        foreach ($itemsArray as $itemName) {
            /** @var Item $itemObject */
            $itemObject = $this->em->getRepository(Item::class)->findOneBy(['name' => trim($itemName)]);
            if ($itemObject) {
                $checkout->scanItem($itemObject);
            }
        }

        return $checkout;
    }
}
