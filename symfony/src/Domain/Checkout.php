<?php declare(strict_types=1);

namespace App\Domain;

use App\Entity\Company;
use App\Entity\Item;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\EntityManagerInterface;

class Checkout
{
    /**
     * @var array
     */
    private $items = [];

    /**
     * @var string
     */
    private $total = '0';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var array
     */
    private $companies;

    public function __construct(EntityManagerInterface $em, array $companies)
    {
        $this->em = $em;
        $this->companies = $companies;
    }

    public function scanItem(Item $item): self
    {
        $this->addItem($item);
        $this->calculateTotal();

        return $this;
    }

    private function addItem(Item $item): void
    {
        if (array_key_exists($item->getName(), $this->items)) {
            ++$this->items[$item->getName()];
        } else {
            $this->items[$item->getName()] = 1;
        }
    }

    private function calculateTotal(): void
    {
        $total = '0';
        foreach ($this->items as $itemName => $itemsCount) {
            /** @var Item $itemEntity */
            $itemEntity = $this->em->getRepository(Item::class)->findOneBy(['name' => $itemName]);
            if ($itemEntity) {

                $companyEntity = $this->getCompanyByItem($itemEntity);
                if ($companyEntity && is_int($itemsCount) && ($companyEntity->getRequiredCount() <= $itemsCount)) {
                    //How much time company can applies
                    $companyCount = intdiv($itemsCount, $companyEntity->getRequiredCount());
                    //Add company prices
                    $total = bcadd($total, bcmul($companyEntity->getPrice(), (string)$companyCount));
                    //remove counted items from total count
                    $itemsCount -= $companyCount * $companyEntity->getRequiredCount();
                }
                if ($itemsCount > 0) {
                    $total = bcadd($total, bcmul($itemEntity->getPrice(), (string)$itemsCount));
                }
            }
        }

        $this->total = $total;
    }

    public function getTotal(): string
    {
        return $this->total;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * Checks if checkout has applied company for given Item type
     * @param Item $item
     * @return Company|null
     */
    private function getCompanyByItem(Item $item): ?Company
    {
        /** @var string $companyName */
        foreach ($this->companies as $companyName) {
            /** @var Company $company */
            $company = $this->em->getRepository(Company::class)->findOneBy(['name' => $companyName]);
            if ($company && ClassUtils::getClass($company->getItem()) === ClassUtils::getClass($item) && $company->getItem()->getId() === $item->getId()) {
                return $company;
            }
        }

        return null;
    }
}
