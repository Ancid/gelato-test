<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\Item;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        //Item A: price 10, company price x3 = 25
        $item = new Item();
        $item
            ->setName('A')
            ->setPrice('10');
        $manager->persist($item);

        $company = new Company();
        $company
            ->setItem($item)
            ->setRequiredCount(3)
            ->setName('Company A')
            ->setPrice('25');
         $manager->persist($company);

        //Item B: price 100, company price x2 = 150
        $item = new Item();
        $item
            ->setName('B')
            ->setPrice('100');
        $manager->persist($item);

        $company = new Company();
        $company
            ->setItem($item)
            ->setName('Company B')
            ->setRequiredCount(2)
            ->setPrice('150');
        $manager->persist($company);

        //Item C: price 25
        $item = new Item();
        $item
            ->setName('C')
            ->setPrice('25');
        $manager->persist($item);

        $manager->flush();
    }
}
