<?php declare(strict_types=1);

namespace App\Command;

use App\Domain\CheckoutService;
use App\Entity\Company;
use App\Entity\Item;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class CheckoutCommand extends Command
{
    /**
     * @var CheckoutService
     */
    private $checkoutService;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * CheckoutCommand constructor.
     * @param CheckoutService $checkoutService
     */
    public function __construct(CheckoutService $checkoutService, EntityManagerInterface $em)
    {
        parent::__construct();
        $this->checkoutService = $checkoutService;
        $this->em = $em;
    }

    protected function configure(): void
    {
        $this
            ->setName('user:checkout')
            ->setDescription('Creates checkout for user')
            ->setDefinition(array(
                new InputOption('items', '', InputOption::VALUE_REQUIRED, 'The list of items in the format: A,B,C'),
            ));
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    public function interact(InputInterface $input, OutputInterface $output): void
    {
            $questionHelper = $this->getHelper('question');

            $question = new Question("Write items for checkout in the format: A,B,C\n");
            $format = $questionHelper->ask($input, $output, $question);
            $input->setOption('items', $format);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $companies = ['Company A', 'Company B'];
        $checkout = $this->checkoutService->processCheckout($input->getOption('items'), $companies);

        //This logic doesn't perform real calculation. Used only for showing results and not covered with any tests
        $items = $checkout->getItems();
        $itemLine = [];
        foreach ($items as $itemName => $itemsCount) {
            $total = '0';
            /** @var Item $itemEntity */
            $itemEntity = $this->em->getRepository(Item::class)->findOneBy(['name' => trim($itemName)]);
            $companyApplied = false;
            $itemsLeft = $itemsCount;
            foreach ($companies as $companyName) {
                /** @var Company $company */
                $companyEntity = $this->em->getRepository(Company::class)->findOneBy(['name' => $companyName]);
                if ($companyEntity && $companyEntity->getItem()->getId() === $itemEntity->getId() &&
                    (ClassUtils::getClass($companyEntity->getItem()) === ClassUtils::getClass($itemEntity))) {
                    if (($companyEntity->getRequiredCount() <= $itemsCount)) {
                        $companyCount = intdiv($itemsCount, $companyEntity->getRequiredCount());
                        $total = bcmul($companyEntity->getPrice(), (string)$companyCount);
                        $itemsLeft = $itemsCount - $companyCount * $companyEntity->getRequiredCount();
                        $companyApplied = true;
                    }
                }

                $priceOutput = bcadd($total, bcmul($itemEntity->getPrice(), (string)$itemsLeft));
                if ($companyApplied) {
                    $priceOutput .= ' ('.$companyName.' applied '.$companyCount.' times)';
                }

            }

            $itemLine[] = $itemName . ' x ' . $itemsCount . '     ' . $priceOutput;
        }
        $output->writeln([
            "\n",
            'Your checkout',
            '-------------',
        ]);
        $output->writeln($itemLine);
        $output->writeln([
            '-------------',
            'Checkout total: '.$checkout->getTotal(),
            "\n",
        ]);

        return Command::SUCCESS;
    }
}
